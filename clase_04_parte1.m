grado = input("Ingrese el grado del polinomio");
minimo = input("Ingrese el valor mínimo del intervalo");
maximo = input("Ingrese el valor máximo del intervalo");
h = input("Ingrese el valor del incremento (h)");
%los arreglos se cuentan desde 1
for i = 1:(grado+1)
  coeficientes(i) = input(strcat("Ingrese el valor para a", int2str(i), ": "));
endfor
j = minimo;
disp("x | y");
k = 1;
while (j <= maximo)
  suma = 0;
  for i = 1:(grado+1)
    suma = suma + (coeficientes(i) * (j^(grado - (i-1))));
  endfor
  x(k) = j;
  y(k) = suma;
  disp(strcat(int2str(j), " | ", int2str(suma)));
  j = j + h;
  k = k + 1;
endwhile
k = k - 1;
l = 1;
for i = 1:(k)
  if y(i) == 0
   raices(l) = x(i);
   disp(strcat("x", int2str(l), ": ", int2str(x(i))));
   l = l + 1;
  endif
endfor
j = 0;
i = 1;
m = 1;
m1 = 1;
n = 1;
n1 = 1;
while (j < grado)
  while (i < k && y(i) > y(i+1))
    decrecientex(n) = x(i);
    decrecientey(n) = y(i);
    n = n + 1;
    i=i+1;
  endwhile
  if n > 1
    decrecientex(n) = x(i);
    decrecientey(n) = y(i);
    disp(strcat("La función es decreciente desde el punto: (", int2str(decrecientex(n1)), ", ", int2str(decrecientey(n1)), ") hasta el punto: (", int2str(decrecientex(n)), ", ", int2str(decrecientey(n)), ")"));
    m1 = m;
    if i < k && y(i) == y(i+1)
      crecientex(m) = x(i);
      crecientey(m) = y(i);
      m = m + 1;
      i=i+1;
    endif
    j = j+1;
  endif
  while (i < k && y(i) < y(i+1))
    crecientex(m) = x(i);
    crecientey(m) = y(i);
    m = m + 1;
    i=i+1;
  endwhile
  if m > 1
    crecientex(m) = x(i);
    crecientey(m) = y(i);
    disp(strcat("La función es creciente desde el punto: (", int2str(crecientex(m1)), ", ", int2str(crecientey(m1)), ") hasta el punto: (", int2str(crecientex(m)), ", ", int2str(crecientey(m)), ")"));  
    n1 = n;    
    if i < k && y(i) == y(i+1)
      decrecientex(n) = x(i);
      decrecientey(n) = y(i);
      n = n + 1;
      i=i+1;
    endif
    j = j+1;
  endif
endwhile

