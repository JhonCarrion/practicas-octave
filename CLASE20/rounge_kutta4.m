function [y] = rounge_kutta4(x, h, y)
k = 1;
h_2 = 0;
dos = 1;
suma = 0;
for j=1:3
  k = dos * diferencial(x + h_2, y + (h_2 * k));
  dos = 2;
  suma = suma + k;
  h_2 = h/2;
endfor
suma = suma + diferencial(x + h, y + (h * k));
y = y + ((h * suma) / 6);
endfunction