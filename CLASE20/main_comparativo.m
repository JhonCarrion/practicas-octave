clear; clc;
format long;

[x, x_n, n, y] = pedir();

h = (x_n - x(1)) / n;
h

y_kutta3(1) = y;
y_kutta4(1) = y;

fprintf('\n');
fprintf(' %5c\t', "X");
fprintf(' %5s\t', "Kutta4");
%fprintf(' %5s\t', "Real");
%fprintf(' %6s\t', "Error_Kutta3");
%fprintf(' %6s\t', "Error_Kutta4");

for i=1:(n + 1)
 % y_real(i) = solex(x(i));
  fprintf('\n');
  fprintf(' %5.4f\t', x(i));
  fprintf(' %5.4f\t', y_kutta3(i));
  fprintf(' %5.4f\t', y_kutta4(i));
%  fprintf(' %5.4f\t', y_real(i));
%  fprintf(' %7.4f\t', abs(y_kutta3(i) - y_real(i)));
%  fprintf(' %7.4f\t', abs(y_kutta4(i) - y_real(i)));
  
  x(i+1) = x(i) + h;
  
  y_kutta3(i+1) = rounge_kutta3(x(i), h, y_kutta3(i));
  y_kutta4(i+1) = rounge_kutta4(x(i), h, y_kutta4(i));
endfor 
fprintf('\n');