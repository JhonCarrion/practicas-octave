function [y_solex] = solex(x)
  y_solex = ((x+1)^2) - (0.5 * exp(x));
  %y_solex = (exp(-0.5 * x) / 13) * ((40 * exp(1.3x)) - 14);
endfunction