clear; clc;
%filas = input("Ingrese el numero de filas");
%columnas = input("Ingrese el numero de columnas");
%error_method = input("Ingrese el valor del error");
%disp("Ingresar valores de la matriz");
%for i = 1:filas
%  for j = 1:columnas
%    coeficientes(i, j) = input(strcat("Ingrese el valor para la posicion (", int2str(i), ", ", int2str(j), "): "));
%    if i == j
%      diagonal(i) = coeficientes(i, j);
%    endif
%  endfor
%endfor
%disp("Ingresar valores de los terminos independientes");
%for i = 1:columnas
%  independientes(i) = input(strcat("Ingrese el termino independiente de la ", int2str(i), " ecuación: "));
%endfor
%disp("Ingresar valores de las aproximaciones");
%for i = 1:columnas
%  aproximaciones(i) = input(strcat("Ingrese el ", int2str(i), " valor: "));
%endfor
coeficientes = [9, 2, -1; 7, 8, 5; 3, 4, -10];
independientes = [-2; 3; 6];
aproximaciones = [0; 0; 0];
resultados = aproximaciones;
error_method = 0.0001;
n_iteraciones = 100;
e = 1;
cont = 0;
while e > error_method && cont < n_iteraciones
  for i = 1:length(coeficientes)
    suma = 0;
    for j = 1:length(coeficientes)
      if j ~= i
        suma = suma + coeficientes(i, j) * aproximaciones(j);
      endif
    endfor
    resultados(i) = (independientes(i) - suma) / coeficientes(i, i);
    e = norm(aproximaciones - resultados);
    aproximaciones = resultados;
  endfor
  cont = cont + 1;
endwhile
disp(cont); 
resultados
disp("caso 2");
coeficientes = [8, -3, 1, -1; -1, 20, -3, 8; 1, 1, 3, -1; 6, -4, 11, 15];
independientes = [20; 3; 3; 9];
aproximaciones = [0; 0; 0; 0];
resultados = aproximaciones;
error_method = 0.00001;
n_iteraciones = 100;
e = 1;
cont = 0;
while e > error_method && cont < n_iteraciones
  for i = 1:length(coeficientes)
    suma = 0;
    for j = 1:length(coeficientes)
      if j ~= i
        suma = suma + coeficientes(i, j) * aproximaciones(j);
      endif
    endfor
    resultados(i) = (independientes(i) - suma) / coeficientes(i, i);
    e = norm(aproximaciones - resultados);
    aproximaciones = resultados;
  endfor
  cont = cont + 1;
endwhile
disp(cont); 
resultados