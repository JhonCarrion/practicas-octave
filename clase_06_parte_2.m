clear; clc;
grado = input("Ingrese el grado del polinomio");
minimo = input("Ingrese el valor mínimo del intervalo");
maximo = input("Ingrese el valor máximo del intervalo");
n = input("Ingrese el numero de iteraciones");
h = (maximo - minimo) / n;
printf("h: %d", h);
disp("");
j = minimo -(h*2) ;
disp("");
for i = 1:(grado+1)
  coeficientes(i) = input(strcat("Ingrese el valor para a", int2str(i), ": "));
endfor
disp("x | y");
k = 1;
while (j <= (maximo +(h*2)))
  suma = 0;
  for i = 1:(grado+1)
    suma = suma + (coeficientes(i) * (j^(grado - (i-1))));
  endfor
  x(k) = j;
  y(k) = suma;
  if j >= minimo && j <= maximo
    disp(strcat(int2str(x(k)), " | ", int2str(y(k))));
  endif
  j = j + h;
  k = k + 1;
endwhile
l = 3;
disp("dy");
while (l < (k -2))
  dy(l) = (-y(l+2)+8*y(l+1)-8*y(l-1)+y(l-2))/12*h;
  disp(dy(l));
  l = l+1;
endwhile

