function [y] = ecuacion(x, grado, coeficientes)
  suma = 0;
  for i = 1:(grado+1)
    suma = suma + (coeficientes(i) * (x^(grado - (i-1))));
  endfor
  y = suma;
endfunction
