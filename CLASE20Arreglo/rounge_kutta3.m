function [y] = rounge_kutta3(x, h, y)
k_1 = diferencial(x, y);
k_2 = diferencial(x + (h/2), y + ((k_1 * h) / 2));
k_3 = diferencial(x + h, y - (h * k_1) + (2 * k_2 * h));
y = y + ((h * (k_1 + (4 * k_2) + k_3)) / 6);
endfunction