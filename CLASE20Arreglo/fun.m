function [dy] = fun(x, y)
  dy = [y(2); y(3); -2*y(3)+y(2)-3*y(1)+2*x];
endfunction
