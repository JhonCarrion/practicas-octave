function [x, y] = rk4(f, a, b, y0, N)
  h=(b-a)/N;
  x=a:h:b;
  x=x(:);
  y=zeros(N+1, length(y0));
  y(1, :) = y0;

  fprintf('\n');
  fprintf(' %5c\t', "X");
  fprintf(' %5c\t', "y");
  for k = 1:N
    fprintf('\n');
    fprintf(' %5.4f\t', x(k));
    
    k1 = feval(f, x(k), y(k, :))';
    k2 = feval(f, x(k)+h/2, y(k, :)+(h/2)*k1)';
    k3 = feval(f, x(k)+h/2, y(k, :)+(h/2)*k2)';
    k4 = feval(f, x(k+1), y(k, :)+h*k3)';
    
    y(k+1, :) = y(k, :) +h/6*(k1+2*k2+2*k3+k4);
    
    fprintf(' %5.4f\t', y(k+1, :));
  endfor
  fprintf('\n');
endfunction