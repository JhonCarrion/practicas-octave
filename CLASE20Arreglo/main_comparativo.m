clear; clc;
format long;

[x, x_n, n, y] = pedir();

h = (x_n - x(1)) / n;
h

y_kutta3(1) = y;

fprintf('\n');
fprintf(' %5c\t', "X");

for i=1:(n + 1)
 % y_real(i) = solex(x(i));
  fprintf('\n');
  fprintf(' %5.4f\t', x(i));
  fprintf(' %5.4f\t', y_kutta3(i));
  
  x(i+1) = x(i) + h;
  
  y_kutta3(i+1) = rounge_kutta3(x(i), h, y_kutta3(i));
endfor 
fprintf('\n');