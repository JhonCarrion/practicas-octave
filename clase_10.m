clear; clc;
A = [1, 3; 2, 4];
B = [1, 0; 0, 1];
multiplicador = 1;
for k=1:size(A, 2)
  disp(strcat("col ", int2str(k)));
  for i=1:size(A, 1)
    multiplicador = A(i, 1);
    for j=1:size(A, 2)
      if k == i || (k == 1 && i == 1)
        printf("A(%d, %d) = %d / %d", i, j, A(i, j), A(k, k));
        disp("");
        A(i, j) = A(i, j) / A(k, k);
        disp(A(i, j));
      else
        pos = i;
        if pos > 1
          pos = i-1;
        endif
        printf("A(%d, %d) = %d - %d * %d", i, j, A(i, j), multiplicador, A(pos, j));
        disp("");
        A(i, j) = A(i, j) - (multiplicador * A(pos, j));
        disp(A(i, j));
      endif
    endfor
    for j=1:size(A, 2)
      if k == i || (k == 1 && i == 1)
        printf("B(%d, %d) = %d / %d", i, j, B(i, j), B(k, k));
        disp("");
        B(i, j) = B(i, j) / A(k, k);
        disp(B(i, j));
      else
        pos = i;
        if pos > 1
          pos = i-1;
        endif
        printf("B(%d, %d) = %d - %d * %d", i, j, B(i, j), multiplicador, B(pos, j));
        disp("");
        B(i, j) = B(i, j) - (multiplicador * B(pos, j));
        disp(B(i, j));
      endif
    endfor
  endfor
endfor
