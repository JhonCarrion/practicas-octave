grado = input("Ingrese el grado del polinomio");
minimo = input("Ingrese el valor mínimo del intervalo");
maximo = input("Ingrese el valor máximo del intervalo");
h = input("Ingrese el valor del incremento (h)");
%los arreglos se cuentan desde 1
for i = 1:(grado+1)
  coeficientes(i) = input(strcat("Ingrese el valor para a", int2str(i), ": "));
endfor
j = minimo;
disp("x | y");
k = 1;
l = 1;
while (j <= maximo)
  suma = 0;
  for i = 1:(grado+1)
    suma = suma + (coeficientes(i) * (j^(grado - (i-1))));
  endfor
  x(k) = j;
  y(k) = suma;
  if suma == 0
    raices(l) = j;
    l = l + 1;
  endif
  disp(strcat(int2str(j), " | ", int2str(suma)));
  j = j + h;
  k = k + 1;
endwhile
k = k - 1;
xAux = x;
yAux = y;
m = 1;
mas = 0;
while (l <= grado)
  o=1;
  if m < k && ((yAux(m) < 0 && yAux(m+1) > 0) || (yAux(m) > 0 && yAux(m+1) < 0))
    a = xAux(m) + mas;
    b = xAux(m + 1) + mas;
    n = a;
    h = h / 2;
    mas = h;
    while (n <= b)
      suma = 0;
      for i = 1:(grado+1)
        suma = suma + (coeficientes(i) * (n^(grado - (i-1))));
      endfor
      xAux(o) = n;
      yAux(o) = suma;
      o=i+1;
      if suma == 0
        raices(l) = n;
        disp("raiz:");
        disp(raices(l));
        l = l + 1;
      endif
      n = n + h;
    endwhile
    k = o - 1;
  endif
  m = m + 1;
endwhile
for i = 1:(l-1)
  disp(strcat("X", int2str(i), ": ", int2str(raices(i))));
endfor



