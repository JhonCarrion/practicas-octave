clear; clc;
disp("Ingresar vector de temperatura en formato: [150; 160; 170; 180]");
temperaturas = input("Temperaturas: ");
disp("Ingresar vector de resistencia en formato: [35.5; 37.8; 43.6; 45.7]");
resistencias = input("Resistencias: ");
x = input("Ingrese el valor de x: ");

evaluando = 1;
resultado = 0;
for j=1:length(temperaturas)
  numerador = 1;
  denominador = 1;
  for i=1:length(temperaturas)
    if i != evaluando
      numerador = numerador * (x - temperaturas(i));
      denominador = denominador * (temperaturas(evaluando) - temperaturas(i));
    endif 
  endfor
  resultado = resultado + ((numerador / denominador) * resistencias(j));
  evaluando = evaluando + 1;
endfor
disp(strcat("Resultado para ", int2str(x), ": "));
disp(resultado);