clear; clc;
format long;

[x, x_n, n, y] = pedir();

h = (x_n - x(1)) / n;
h

y_kutta3(1) = y;
y_kutta4(1) = y;
y_euler(1) = y;
y_heun(1) = y;

fprintf('\n');
fprintf(' %5c\t', "X");
fprintf(' %5s\t', "Euler");
fprintf(' %5s\t', "Heun");
fprintf(' %5s\t', "Kutta3");
fprintf(' %5s\t', "Kutta4");
%fprintf(' %5s\t', "Real");
%fprintf(' %5s\t', "Error_Euler");
%fprintf(' %6s\t', "Error_Heun");
%fprintf(' %5s\t', "Error Kutta3");
%fprintf(' %6s\t', "Error_Kutta4");

for i=1:(n + 1)
%  y_real(i) = solex(x(i));
  fprintf('\n');
  fprintf(' %5.4f\t', x(i));
  fprintf(' %5.4f\t', y_euler(i));
  fprintf(' %5.4f\t', y_heun(i));
  fprintf(' %5.4f\t', y_kutta3(i));
  fprintf(' %5.4f\t', y_kutta4(i));
%  fprintf(' %5.4f\t', y_real(i));
%  fprintf(' %7.4f\t', abs(y_euler(i) - y_real(i)));
%  fprintf(' %7.4f\t', abs(y_heun(i) - y_real(i)));
%  fprintf(' %7.4f\t', abs(y_kutta4(i) - y_real(i)));
  
  x(i+1) = x(i) + h;
  
  y_euler(i+1) = euler(x(i), h, y_euler(i));
  y_heun(i+1) = heun(x(i+1), x(i), h, y_heun(i));
  y_kutta3(i+1) = rounge_kutta3(x(i), h, y_kutta3(i));
  y_kutta4(i+1) = rounge_kutta4(x(i), h, y_kutta4(i));
endfor 
fprintf('\n');
plot(x, y_heun, "g;Heun;");
hold on
plot(x, y_euler, "r;Euler;");
plot(x, y_kutta3, "k;Kutta3;");
plot(x, y_kutta4, "b;Kutta4;");
hold off
