function [y_p] = diferencial(x, y)
  %y_p = y - (x^2) + 1;
  %y_p = (4 * exp(0.8 * x)) - (0.5 * y);
  %y_p = cos(2*x) + sin(3*x);
  y_p = (-2*pi()*sin(2*pi()*x)) - ((1/0.001)*(y-cos(2*x)));
endfunction