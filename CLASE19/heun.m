function [y] = heun(x, x_old, h, y_old)
  y = y_old + (h * diferencial(x_old, y_old));
  y = y_old + (((h * ((diferencial(x_old, y_old) + diferencial(x, y))))) / 2);
endfunction