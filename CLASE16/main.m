clear; clc;
format long;
[x, x_n, n, y] = pedir();
h = (x_n - x(1)) / n;
h
disp("x | y");
printf("%d  | %d \n", x(1), y(1));
for i=2:(n + 1)
  x(i) = x(i - 1) + h;
  y(i) = y(i - 1) + (h * diferencial(x(i - 1), y(i - 1)));
  printf("%d  | %d \n", x(i), y(i));
endfor  
plot(x, y);