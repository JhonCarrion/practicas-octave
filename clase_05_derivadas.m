grado = input("Ingrese el grado del polinomio");
%los arreglos se cuentan desde 1
g = grado;
polinomio = ""; 
for i = 1:(grado+1)
  signo = " + ";
  coeficientes(i) = input(strcat("Ingrese el valor para a", int2str(i), ": "));
  if coeficientes(i) < 0
    signo = " ";
  endif
  if strcmp(polinomio, "") == 0
    polinomio = strcat(polinomio, signo);
  endif
  if g == 0
    polinomio = strcat(polinomio, int2str(coeficientes(i)));
  else
    polinomio = strcat(polinomio, int2str(coeficientes(i)), "x^", int2str(g));
  endif
  g= g - 1;
endfor
g = grado;
gradoAux = grado;
coeficientesDerivada = coeficientes;
do
  salir = 0;
  disp(strcat("Calcular la derivada de la función: ", polinomio));
  g = g - 1;
  disp(strcat("Ingrese 1 para calcular la:", int2str(grado - g), " derivada"));
  disp("Ingrese 2 para salir");
  opt = input("Ingrese su opción: ");
  if opt == 2
    disp("¡GRACIAS!");
    salir = 1;
  else
    derivada = "";
    menos = gradoAux -1;
    pow = gradoAux;
    for i = 1:gradoAux
      signo = " + ";
      coeficientesDerivada(i) = coeficientesDerivada(i) * pow;
      if coeficientesDerivada(i) < 0
        signo = " ";
      endif
      if strcmp(derivada, "") == 0
        derivada = strcat(derivada, signo);
      endif
      if menos == 0
        derivada = strcat(derivada, int2str(coeficientesDerivada(i)));
      else
        derivada = strcat(derivada, int2str(coeficientesDerivada(i)), "x^", int2str(menos));
      endif
      menos = menos - 1;
      pow = pow - 1;
    endfor
    gradoAux = gradoAux -1;
    disp("****** LA DERIVADA ES: ******");
    disp(derivada);
    disp("*****************************");
    if (grado - g) == grado
      salir = 1;
      disp("Ya no se puede seguir derivando");
      disp("¡GRACIAS!");
    endif
  endif
until(salir == 1)
