clear; clc;
function [fx] = progresiva(y, pos, h)    
  fx = ((2*y(pos)) - (5*y(pos+1)) + (4*y(pos+2)) - (y(pos+3))) / (h^2);
endfunction

function [fx] = centrada(y, pos, h)    
  fx = (-y(pos-2) + (16*y(pos-1)) - (30*y(pos)) + (16*y(pos+1)) - y(pos+2)) / (12*(h^2));
endfunction

function [fx] = regresiva(y, pos, h)    
  fx = (-y(pos-3) + (4*y(pos-2)) - (5*y(pos-1)) + (2*y(pos))) / (h^2);
endfunction
minimo = input("Ingrese el valor mínimo del intervalo");
maximo = input("Ingrese el valor máximo del intervalo");
n = input("Ingrese el numero de iteraciones");
h = (maximo - minimo) / n;
printf("h: %d", h);
disp("");
disp("P = Progresivas");
disp("C = Centradas");
disp("R = Regresivas");
k = 1;
j = (minimo - (3*h));
posin = 0;
posout = 0;
disp("x | y | P | C | R");
while (j <= (maximo + (3*h)))
  x(k) = j;
  y(k) = ecuacion(j);
 if j <= minimo && (j +h) > minimo
   posin = k;
 endif
 if j <= maximo && (j +h) > maximo
   posout = k;
 endif 
 j = j + h;
 k = k + 1;
endwhile
l = 1;
j = posin;
while (j <= posout)
  progre(l) = progresiva(y, j, h);
  cent(l) = centrada(y, j, h);
  regre(l) = regresiva(y, j, h);
  printf("%d | %d | %d | %d | %d", x(j), y(j), progre(l), cent(l), regre(l));
  disp("");
  k = k+1;
  j = j+1;
  l = l+1;
endwhile