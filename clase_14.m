clear; clc;
format long;
pkg load symbolic
x0 = input("Ingrese el valor de x0: ");
x = input("Ingrese el valor de x: ");
n = input("Ingrese el grado de la ecuacion: ");
function [fx] = funcion(x, x0, i)
  syms a;
  f = exp(a);
  fx = (diff(f, i)*((x - x0)^i)) / factorial(i);
endfunction
suma = 0;
for i=0:n
  suma = suma + funcion(x, x0, i);
endfor
disp(strcat("El valor de Y para: ", int2str(x)));
disp(suma);
