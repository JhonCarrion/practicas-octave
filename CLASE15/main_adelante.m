clear; clc;

[x, h, x_array, y_array] = caso_2();

s = (x - x_array(1)) / h;

if length(x_array) == length(y_array)
  array = y_array;
  j = 1;
  %deltas
  while (j < length(y_array))
    deltas_aux = [];
    for i=1:length(array)
      if (i + 1) <= length(array)
        deltas_aux(i) = array(i + 1) - array(i);
      endif 
    endfor
    array = deltas_aux;
    disp(array);
    deltas(j) = deltas_aux(1);
    j = j + 1;
  endwhile
  %fin delta
  px = y_array(1) + (deltas(1) * s);
  for i=2:length(deltas)
    numerador = deltas(i) * s;
    for j=1:(i-1);
      numerador = numerador * (s - j);
    endfor
    px = px + (numerador / factorial(i));
  endfor
  disp("El resultado es:");
  disp(px);
else
  disp("Los vectores x y y no teiene el mismo tama�o.");
endif  