function [x, h, x_array, y_array] = pedir()
  x = input("Ingrese el valor a calcular (x): ");
  h = input("Ingrese el valor de h: ");
  disp("Para los siguientes datos debe ingresarlos co la forma [n1; n2; n3], tenga en cuenta que los vectores x y y deben tener la misma cantidad de datos");
  disp("Ejemplo:");
  disp("[1; 1.67; 2.33; 3]");
  x_array = input("Ingrese el vector x: ");
  y_array = input("Ingrese el vector y (Delta 0): ");
endfunction 