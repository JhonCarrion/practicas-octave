clear; clc;
function [x, x_array, y_array] = pedir()
  x = input("Ingrese el valor a calcular (x): ");
  disp("Para los siguientes datos debe ingresarlos co la forma [n1; n2; n3], tenga en cuenta que los vectores x y y deben tener la misma cantidad de datos");
  disp("Ejemplo:");
  disp("[1; 1.67; 2.33; 3]");
  x_array = input("Ingrese el vector x: ");
  y_array = input("Ingrese el vector y (Delta 0): ");
endfunction 

function [x, x_array, y_array] = caso_1()
  x = 2.1;
  x_array = [1; 1.67; 2.33; 3];
  y_array = [1.99; 1.76; 1.69; 1.63];
endfunction

function [x, x_array, y_array] = caso_2()
  x = 2;
  x_array = [3.1; 2.8; 2.5; 2.2; 1.9; 1.6];
  y_array = [2.31; 2.54; 2.82; 3.21; 3.99; 4.75];
endfunction

[x, x_array, y_array] = caso_2();

%Calcular el valor de h
array_x = x_array;
j = 1;
while (j < 3)
  x_aux = [];
  for i=1:length(array_x)
    if (i + 1) <= length(array_x)
      x_aux(i) = array_x(i + 1) - array_x(i);
    endif 
  endfor
  array_x = x_aux;
  j = j + 1;
endwhile
for i=1:length(array_x)
  if array_x(i) <= 0.01 && array_x(i) >= -0.01
    pasar = true;
  else
    pasar = false;
    break;
  endif
endfor

if pasar
  h = x_array(2)-x_array(1);
  s = (x - x_array(length(x_array))) / h;

  if length(x_array) == length(y_array)
    array = y_array;
    j = 1;
    %deltas
    while (j < length(y_array))
      deltas_aux = [];
      for i=1:length(array)
        if (i + 1) <= length(array)
          deltas_aux(i) = array(i + 1) - array(i);
        endif 
      endfor
      array = deltas_aux;
      disp(array);
      deltas(j) = deltas_aux(length(deltas_aux));
      j = j + 1;
    endwhile
    %fin delta
    px = y_array(length(y_array)) + (deltas(1) * s);
    for i=2:length(deltas)
      numerador = deltas(i) * s;
      for j=1:(i-1);
        numerador = numerador * (s + j);
      endfor
      px = px + (numerador / factorial(i));
    endfor
    disp("El resultado es:");
    disp(px);
  else
    disp("Los vectores x y y no teiene el mismo tama�o.");
  endif 
else
  disp("Las X no son equidistantes.");
endif
 