clear; clc;
[coeficientes, independientes, aproximaciones, error_method, n_iteraciones] = pedir();
resultados = aproximaciones;
e = 1;
cont = 0;
while e > error_method && cont < n_iteraciones
  for i = 1:length(coeficientes)
    suma = 0;
    for j = 1:length(coeficientes)
      if j ~= i
        suma = suma + coeficientes(i, j) * aproximaciones(j);
      endif
    endfor
    resultados(i) = (independientes(i) - suma) / coeficientes(i, i);
    e = norm(aproximaciones - resultados);
    aproximaciones = resultados;
  endfor
  cont = cont + 1;
endwhile
disp(cont); 
resultados
