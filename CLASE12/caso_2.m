function [coeficientes, independientes, aproximaciones, error_method, n_iteraciones] = caso_2()
  coeficientes = [8, -3, 1, -1; -1, 20, -3, 8; 1, 1, 3, -1; 6, -4, 11, 15];
  independientes = [20; 3; 3; 9];
  aproximaciones = [0; 0; 0; 0];
  error_method = 0.00001;
  n_iteraciones = 100;
endfunction