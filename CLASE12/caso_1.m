function [coeficientes, independientes, aproximaciones, error_method, n_iteraciones] = caso_1()
  coeficientes = [9, 2, -1; 7, 8, 5; 3, 4, -10];
  independientes = [-2; 3; 6];
  aproximaciones = [0; 0; 0];
  error_method = 0.00001;
  n_iteraciones = 100;
endfunction
