function [coeficientes, independientes, aproximaciones, error_method, n_iteraciones] = pedir()
  filas = input("Ingrese el numero de filas");
  columnas = input("Ingrese el numero de columnas");
  error_method = input("Ingrese el valor del error");
  n_iteraciones = input("Ingrese el numero maximo de iteraciones");
  disp("Ingresar valores de la matriz");
  for i = 1:filas
    for j = 1:columnas
      coeficientes(i, j) = input(strcat("Ingrese el valor para la posicion (", int2str(i), ", ", int2str(j), "): "));
      if i == j
        diagonal(i) = coeficientes(i, j);
      endif
    endfor
  endfor
  disp("Ingresar valores de los terminos independientes");
  for i = 1:columnas
    independientes(i) = input(strcat("Ingrese el termino independiente de la ", int2str(i), " ecuación: "));
  endfor
  disp("Ingresar valores de las aproximaciones");
  for i = 1:columnas
    aproximaciones(i) = input(strcat("Ingrese el ", int2str(i), " valor: "));
  endfor
endfunction