function [y] = ecua(x)
 y = x^4*(5-x^2);
endfunction