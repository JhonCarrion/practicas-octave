clear; clc;
minimo = input("Ingrese el valor mínimo del intervalo");
maximo = input("Ingrese el valor máximo del intervalo");
n = input("Ingrese el numero de iteraciones");
error_metodo = input("Ingrese el valor del error");
function [] = iterar(minimo, maximo, n, error_metodo, metodo)
 if metodo == 1
  iteracion1 = clase_07_integral(minimo, maximo, n);
 elseif metodo == 2
  n = n*2;
  iteracion1 = clase_07_integrar_parte_2(minimo, maximo, n);
 else
  iteracion1 = clase_07_integrar_parte_3(minimo, maximo, n);
 endif
 
 n = n*2;
 if metodo == 1
  iteracion2 = clase_07_integral(minimo, maximo, n);
 elseif metodo == 2
  iteracion2 = clase_07_integrar_parte_2(minimo, maximo, n);
 else
  iteracion2 = clase_07_integrar_parte_3(minimo, maximo, n);
 endif
 
 cont =1;
 while abs(iteracion2 - iteracion1) > error_metodo
   iteracion1 = iteracion2;
   n = n*2;
   if metodo == 1
    iteracion2 = clase_07_integral(minimo, maximo, n);
   elseif metodo == 2
    iteracion2 = clase_07_integrar_parte_2(minimo, maximo, n);
   else
    iteracion2 = clase_07_integrar_parte_3(minimo, maximo, n);
   endif
   cont = cont +1;  
 endwhile
 disp("*******************");
 if metodo == 1
  disp("Trapecios");
 elseif metodo == 2  
  disp("Simpson 1/3");
 else
  disp("Simpson 3/8");
 endif
 disp(strcat("Iteraciones: ", int2str(cont)));
 disp("la integral final es:");
 disp(iteracion2); 
 disp("*******************");
endfunction
iterar(minimo, maximo, n, error_metodo, 1);
iterar(minimo, maximo, n, error_metodo, 2);
iterar(minimo, maximo, n, error_metodo, 3);
