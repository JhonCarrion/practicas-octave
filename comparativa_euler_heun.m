clear; clc;
format long;
function [x, x_n, n, y] = pedir()
  x(1) = input("Ingrese el primer valor (a) de x: ");
  x_n = input("Ingrese el ultimo valor (b) de x: ");
  n = input("Ingrese el valor de N: ");
  y(1) = input("Ingrese el primer valor de y: ");
endfunction

function [y_p] = diferencial(x, y)
  y_p = y - (x^2) + 1;
endfunction
function [y_solex] = solex(x)
  y_solex = ((x+1)^2) - (0.5 * exp(x));
endfunction


[x, x_n, n, y] = pedir();
h = (x_n - x(1)) / n;
h
y_euler(1) = y(1);
y_real(1) = y(1);
disp("x |   euler |  heun  |  real  |  Error_euler   |   error_heun");
printf("%d  | %d  |   %d  | %d  | %d  | %d\n", x(1), y_euler(1), y(1), y_real(1), 0, 0);
for i=2:(n + 1)
  x(i) = x(i - 1) + h;
  y_euler(i) = y_euler(i - 1) + (h * diferencial(x(i - 1), y_euler(i - 1)));
  y(i) = y(i - 1) + (h * diferencial(x(i - 1), y(i - 1)));
  y(i) = y(i - 1) + (((h * ((diferencial(x(i - 1), y(i - 1)) + diferencial(x(i), y(i)))))) / 2);
  y_real(i) = solex(x(i));
  printf("%d  | %d  |   %d  | %d  | %d  | %d\n", x(i), y_euler(i), y(i), y_real(i), (y_real(i) - y_euler(i)), (y_real(i) - y(i)));
endfor 
plot(x, y, "g;heun;");
hold on
plot(x, y_euler, "r;euler;");
plot(x, y_real, "k;Real;")
hold off