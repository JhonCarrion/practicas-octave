clear; clc;
x0 = input("Ingrese el valor de X0: ");
error_metodo = input("Ingrese el valor del error: ");
iteracion1 = x0 - (funcion(x0) / derivada(x0));
iteracion2 = iteracion1 - (funcion(iteracion1) / derivada(iteracion1));
cont =2;
while abs(iteracion2 - iteracion1) > error_metodo
 iteracion1 = iteracion2;
 iteracion2 = iteracion1 - (funcion(iteracion1) / derivada(iteracion1));
 cont = cont +1;  
endwhile
 disp("*******************");
 disp(strcat("Iteraciones: ", int2str(cont)));
 disp("La raiz aproximada es:");
 disp(iteracion2); 
 disp("*******************");