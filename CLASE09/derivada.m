function [y] = derivada(x)
  %y = (3*(x^2))-1;
  y = -sin(x)-1;
endfunction
