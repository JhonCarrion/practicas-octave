function [coeficientes] = llenar(grado)
  g = grado;
  polinomio = ""; 
  for i = 1:(grado+1)
    signo = " + ";
    coeficientes(i) = input(strcat("Ingrese el valor para a", int2str(i), ": "));
    if coeficientes(i) < 0
      signo = " ";
    endif
    if strcmp(polinomio, "") == 0
      polinomio = strcat(polinomio, signo);
    endif
    if g == 0
      polinomio = strcat(polinomio, int2str(coeficientes(i)));
    else
      polinomio = strcat(polinomio, int2str(coeficientes(i)), "x^", int2str(g));
    endif
    g= g - 1;
  endfor
  disp(polinomio);
endfunction
