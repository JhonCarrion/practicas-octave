function [temperaturas, resistencias, x] = pedir()
  disp("Ingresar vector de temperatura en formato: [150; 160; 170; 180]");
  temperaturas = input("Temperaturas: ");
  disp("Ingresar vector de resistencia en formato: [35.5; 37.8; 43.6; 45.7]");
  resistencias = input("Resistencias: ");
  x = input("Ingrese el valor de x: ");
endfunction
