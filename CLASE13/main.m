clear; clc;
format long;
[temperaturas, resistencias, x] = caso_1();
evaluando = 1;
resultado = 0;
for j=1:length(temperaturas)
  numerador = 1;
  denominador = 1;
  for i=1:length(temperaturas)
    if i != evaluando
      numerador = numerador * (x - temperaturas(i));
      denominador = denominador * (temperaturas(evaluando) - temperaturas(i));
    endif 
  endfor
  resultado = resultado + ((numerador / denominador) * resistencias(j));
  evaluando = evaluando + 1;
endfor
temperaturas(i+1) = x;
resistencias(j+1) = resultado;
disp(strcat("Resultado para ", int2str(x), ": "));
disp(resultado);
plot(temperaturas, resistencias);